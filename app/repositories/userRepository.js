const { User } = require("../models");

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy(id);
  },

  find(id) {
    return User.findByPk(id);
  },

  findAll() {
    return User.findAll({
      attributes: {
        exclude: ['password']
    }
    });
  },

  getTotalUser() {
    return User.count();
  },

  // register
  async registerNewUser(createArgs) {
    console.log('ini repository')

    const user = await User.findOne({
      where: {
        email : createArgs.email
      }
    })

    // check if user exsits
    if(user){
      if(user.email === createArgs.email){
        throw new Error(`user with email : ${user.email} already taken`)
      }
    }

    console.log('iinnniiiii')

    return User.create(createArgs);
  },

  async login(userArgs) {
    const user = await User.findOne({
      where: {
        email: userArgs
      }
    });
    return user;
  },

};

// module.exports = {
//   create
//   register
//   fafwfwfw
// }
